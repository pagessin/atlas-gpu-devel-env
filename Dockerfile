# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building a Docker image that allows ATLAS developers
# to conveniently develop offline code for GPUs with.
#

# Base the image on vanilla CentOS 7.
FROM centos:7

# Perform the installation as root.
USER root
WORKDIR /root

# Install a modern compiler (courtesy of LCG), and some basic packages necessary
# for ATLAS offline software development. The soft link under /opt/lcg is needed
# for (the current version of) asetup to find the compiler correctly.
COPY docker/lcg-contrib.repo /etc/yum.repos.d/
RUN yum -y install which redhat-lsb-core nano sudo glibc-devel libaio          \
           libX11 libXft libXpm libXext libXi libSM mesa-libGL mesa-libGLU     \
           gcc_8.3.0_x86_64_centos7                                            \
           binutils_2.30_x86_64_centos7 git wget tar                           \
           atlas-devel ctags libX11-devel libXpm-devel libXft-devel            \
           libXext-devel libXi-devel openssl-devel rpm-build gmp-devel         \
           mesa-libGL-devel mesa-libGLU-devel libcurl-devel glib2-devel        \
           xz-devel rpm-build epel-release &&                                  \
    yum clean all &&                                                           \
    ln -s /opt/lcg/gcc/8.3.0/x86_64-centos7 /opt/lcg/gcc/
COPY docker/gcc-8.3.0.sh /etc/profile.d/

# Install CUDA 10.1, without its own OpenCL library.
RUN yum-config-manager --add-repo                                              \
    http://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-rhel7.repo && \
    yum install -y cuda-compat-10-1 cuda-cudart-10-1 cuda-libraries-10-1       \
                   cuda-command-line-tools-10-1 cuda-minimal-build-10-1 &&     \
    echo "/usr/local/cuda-10.1/compat" >                                       \
         /etc/ld.so.conf.d/cuda-10-1-compat.conf &&                            \
    rm /usr/local/cuda-10.1/lib64/libOpenCL.* &&                               \
    ln -s cuda-10.1 /usr/local/cuda &&                                         \
    yum clean all
COPY docker/cuda-10-1.sh /etc/profile.d/
ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility

# Install ROCm 3.8.0.
COPY docker/rocm-3.8.repo /etc/yum.repos.d/
RUN yum install -y rocminfo rocm-smi rocm-device-libs rocm-cmake rocm-gdb      \
                   rocm-utils hip-base hip-rocclr llvm-amdgpu &&               \
    ln -s rocm-3.8.0 /opt/rocm &&                                              \
    yum clean all
COPY docker/rocm-3.8.0.sh /etc/profile.d/

# Install CMake 3.18.3 and Ninja 1.10.1.
RUN wget https://cmake.org/files/v3.18/cmake-3.18.3-Linux-x86_64.tar.gz &&     \
    mkdir -p /opt/cmake/3.18.3/Linux-x86_64 &&                                 \
    tar -C /opt/cmake/3.18.3/Linux-x86_64 --strip-components=1                 \
        --no-same-owner -xvf cmake-*-Linux-x86_64.tar.gz &&                    \
    rm cmake-*-Linux-x86_64.tar.gz &&                                          \
    wget https://github.com/ninja-build/ninja/releases/download/v1.10.1/ninja-linux.zip && \
    mkdir -p /opt/ninja/1.10.1/Linux-x86_64 &&                                 \
    unzip ninja-linux.zip -d /opt/ninja/1.10.1/Linux-x86_64 &&                 \
    chmod 755 /opt/ninja/*/Linux-x86_64/ninja &&                               \
    rm ninja-linux.zip
COPY docker/cmake-3.18.3.sh docker/ninja-1.10.1.sh /etc/profile.d/

# Build a beta version of Intel's Clang compiler.
ARG LLVM_VERSION=36c61d7
ARG LLVM_INSTALL_DIR=/opt/intel-clang/12.0.0-${LLVM_VERSION}/x86_64-centos7
ARG LLVM_SOURCE_DIR=/root/llvm
ARG LLVM_BINARY_DIR=/root/build
ARG GCC_INSTALL_DIR=/opt/lcg/gcc/8.3.0/x86_64-centos7
RUN git clone https://github.com/intel/llvm.git ${LLVM_SOURCE_DIR} &&          \
    cd ${LLVM_SOURCE_DIR}/ && git checkout ${LLVM_VERSION} &&                  \
    mkdir -p ${LLVM_BINARY_DIR} && cd ${LLVM_BINARY_DIR}/ &&                   \
    source ${GCC_INSTALL_DIR}/setup.sh &&                                      \
    export PATH=/opt/ninja/1.10.1/Linux-x86_64${PATH:+:${PATH}} &&             \
    export PATH=/opt/cmake/3.18.3/Linux-x86_64/bin${PATH:+:${PATH}} &&         \
    cmake -G Ninja -DCMAKE_BUILD_TYPE=Release                                  \
       -DCMAKE_INSTALL_PREFIX=${LLVM_INSTALL_DIR}                              \
       -DLLVM_TARGETS_TO_BUILD="X86;NVPTX;AMDGPU"                              \
       -DLLVM_EXTERNAL_PROJECTS="llvm-spirv;sycl;libdevice;opencl-aot"         \
       -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;compiler-rt;llvm-spirv;sycl;libclc" \
       -DLLVM_EXTERNAL_LLVM_SPIRV_SOURCE_DIR=${LLVM_SOURCE_DIR}/llvm-spirv     \
       -DLLVM_EXTERNAL_SYCL_SOURCE_DIR=${LLVM_SOURCE_DIR}/sycl                 \
       -DLLVM_EXTERNAL_LIBDEVICE_SOURCE_DIR=${LLVM_SOURCE_DIR}/libdevice       \
       -DLLVM_ENABLE_EH=ON -DLLVM_ENABLE_PIC=ON -DLLVM_ENABLE_RTTI=ON          \
       -DLLVM_INCLUDE_TESTS=FALSE -DSYCL_BUILD_PI_CUDA=ON                      \
       -DLIBCLC_TARGETS_TO_BUILD="nvptx64--;nvptx64--nvidiacl;amdgcn--;amdgcn--amdhsa" \
       -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-10.1                            \
       -DCMAKE_PREFIX_PATH=/usr/local/cuda-10.1/compat                         \
       -DOpenCL_INCLUDE_DIRS= -DOpenCL_LIBRARIES=                              \
       -DOpenCL_INSTALL_KHRONOS_ICD_LOADER=TRUE                                \
       -DGCC_INSTALL_PREFIX=${GCC_INSTALL_DIR}                                 \
       ${LLVM_SOURCE_DIR}/llvm/ &&                                             \
    cmake --build . ; cmake --build . && cmake --build . -- libsycldevice &&   \
    cmake --install . &&                                                       \
    rm -rf ${LLVM_SOURCE_DIR} ${LLVM_BINARY_DIR} &&                            \
    ln -s 12.0.0-${LLVM_VERSION} /opt/intel-clang/12.0.0 &&                    \
    echo "${LLVM_INSTALL_DIR}/lib" >                                           \
         /etc/ld.so.conf.d/intel-clang.conf &&                                 \
    ldconfig
COPY docker/intel-clang.sh /etc/profile.d/

# Install the Intel OpenCL GPU driver.
RUN yum -y install yum-plugin-copr &&                                          \
    yum -y copr enable jdanecki/intel-opencl &&                                \
    yum -y install level-zero intel-level-zero-gpu intel-opencl clinfo &&      \
    yum clean all

# Build/install the syclinfo executable.
COPY syclinfo/syclinfo.cxx /root/syclinfo.cxx
RUN mkdir -p /opt/syclinfo &&                                                  \
    source ${GCC_INSTALL_DIR}/setup.sh &&                                      \
    export LD_LIBRARY_PATH=${LLVM_INSTALL_DIR}/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}} && \
    ${LLVM_INSTALL_DIR}/bin/clang++ -fsycl -o /opt/syclinfo/syclinfo           \
       /root/syclinfo.cxx &&                                                   \
    rm -f /root/syclinfo.cxx
COPY docker/syclinfo.sh /etc/profile.d/

# Set up the ATLAS user, and give it "all the rights it needs". Note the the
# group IDs are hard-coded to the values that I have on my Ubuntu 20.04 host.
# Unfortunately I just didn't find a way to avoid this, while still not using
# the root user on both the host and in the container to use my AMD GPU...
RUN echo '%wheel        ALL=(ALL)       NOPASSWD: ALL' >> /etc/sudoers &&      \
    groupmod -g 44 video && groupadd -g 109 render &&                          \
    adduser atlas && chmod 755 /home/atlas &&                                  \
    usermod -aG wheel atlas &&                                                 \
    usermod -aG video atlas && usermod -aG render atlas &&                     \
    mkdir /workdir && chown "atlas:atlas" /workdir &&                          \
    chmod 755 /workdir

# Set up the ATLAS specific runtime environment/configuration.
COPY docker/atlas_prompt.sh docker/atlas_env.sh /etc/profile.d/
COPY docker/.asetup /home/atlas/

# Add basic usage instructions for the image.
COPY docker/motd /etc/

# Switch to the ATLAS user.
USER atlas
WORKDIR /workdir

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
