# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Script setting up the Intel-Clang compiler in the image.
#

export PATH=/opt/intel-clang/12.0.0/x86_64-centos7/bin${PATH:+:${PATH}}
export CC=/opt/intel-clang/12.0.0/x86_64-centos7/bin/clang
export CXX=/opt/intel-clang/12.0.0/x86_64-centos7/bin/clang++
