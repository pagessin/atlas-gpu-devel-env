# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for Ninja 1.10.1.
#

export PATH=/opt/ninja/1.10.1/Linux-x86_64${PATH:+:${PATH}}
