# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for ROCm 3.8.0.
#

export PATH=/opt/rocm-3.8.0/bin${PATH:+:${PATH}}
