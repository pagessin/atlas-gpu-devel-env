# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for the custom syclinfo executable.
#

export PATH=/opt/syclinfo${PATH:+:${PATH}}
