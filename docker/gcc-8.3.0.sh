# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Script setting up the LCG GCC 8 compiler in the container.
#

source /opt/lcg/gcc/8.3.0/x86_64-centos7/setup.sh
