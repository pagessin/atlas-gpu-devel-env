# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for CMake 3.18.3.
#

export PATH=/opt/cmake/3.18.3/Linux-x86_64/bin${PATH:+:${PATH}}
