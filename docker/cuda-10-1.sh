# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for CUDA 10.1.
#

export PATH=/usr/local/cuda-10.1/bin${PATH:+:${PATH}}
export CUDACXX=/usr/local/cuda-10.1/bin/nvcc
