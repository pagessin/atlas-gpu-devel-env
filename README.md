# ATLAS GPU Development Environment

This repository holds some code for building a
[Docker](https://www.docker.com/) image that could then be used to develop
"GPU code" for ATLAS, on top of the
[atlas/athena](https://gitlab.cern.ch/atlas/athena) software project(s).

## Image Contents

The image comes with the following packaged inside of it:
  - [GCC 8.3.0](https://gcc.gnu.org/onlinedocs/8.3.0/), coming from:
    http://cern.ch/lcgpackages/rpms_contrib/;
  - [CUDA 10.1](https://docs.nvidia.com/cuda/archive/10.1/), coming from:
    https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64;
  - [ROCm 3.8.0](https://rocmdocs.amd.com/en/latest/), coming from:
    http://repo.radeon.com/rocm/yum/3.8;
  - [CMake 3.18.3](https://cmake.org/), coming from:
    https://cmake.org/files/v3.18/;
  - [Ninja 1.10.1](https://ninja-build.org/), coming from:
    https://github.com/ninja-build/ninja/releases/download/v1.10.1/ninja-linux.zip;
  - [DPC++ Beta](https://spec.oneapi.com/versions/latest/elements/dpcpp/source/index.html),
    built directly out of: https://github.com/intel/llvm;
  - A bunch of CentOS 7 system packages required to serve those installations.

## Starting The Container

Without much discussion, this is how I start the container on a machine that
has no GPU in it:

```
docker run -it --rm \
  --mount "source=/cvmfs,target=/cvmfs,type=bind,bind-propagation=shared,readonly,consistency=cached" \
  -v /afs/cern.ch:/afs/cern.ch:ro -v "$PWD":"$PWD" -w "$PWD" \
  gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:centos7
```

In order to make use of an AMD GPU installed on the host, I execute the
following:

```
docker run -it --rm \
  --mount "source=/cvmfs,target=/cvmfs,type=bind,bind-propagation=shared,readonly,consistency=cached" \
  -v /afs/cern.ch:/afs/cern.ch:ro -v "$PWD":"$PWD" -w "$PWD" \
  --device /dev/kfd --privileged \
  gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:centos7
```

In order to make use of an integrated Intel GPU installed on the host, I
execute the following:

```
docker run -it --rm \
  --mount "source=/cvmfs,target=/cvmfs,type=bind,bind-propagation=shared,readonly,consistency=cached" \
  -v /afs/cern.ch:/afs/cern.ch:ro -v "$PWD":"$PWD" -w "$PWD" \
  --device /dev/dri \
  gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:centos7
```

In order to make use of an NVidia GPU installed on the host, I execute the
following:

```
docker run -it --rm \
  --mount "source=/cvmfs,target=/cvmfs,type=bind,bind-propagation=shared,readonly,consistency=cached" \
  -v /afs/cern.ch:/afs/cern.ch:ro -v "$PWD":"$PWD" -w "$PWD" \
  --gpus all \
  gitlab-registry.cern.ch/akraszna/atlas-gpu-devel-env:centos7
```

Naturally, the options can be combined in case you have multiple of these types
of GPUs available on your host system.

## Using the Container

On startup the container makes all compilers and tools available to the runtime
environment out of the box. Any builds/tests not requiring the ATLAS offline
software environment can just be started right away.

To set up an offline software release/nightly, the container comes set up with
the `setupATLAS` command. Note however that when setting up a release/nightly
with `asetup`, the environment will still be set up to use the Clang compiler
of the image, instead of whatever the release/nightly was built with.

```
[bash][atlas]:gpu-devel-env > setupATLAS
lsetup               lsetup <tool1> [ <tool2> ...] (see lsetup -h):
 lsetup agis          ATLAS Grid Information System
 lsetup asetup        (or asetup) to setup an Athena release
 lsetup atlantis      Atlantis: event display
 lsetup eiclient      Event Index
 lsetup emi           EMI: grid middleware user interface
 lsetup ganga         Ganga: job definition and management client
 lsetup lcgenv        lcgenv: setup tools from cvmfs SFT repository
 lsetup panda         Panda: Production ANd Distributed Analysis
 lsetup pod           Proof-on-Demand (obsolete)
 lsetup pyami         pyAMI: ATLAS Metadata Interface python client
 lsetup root          ROOT data processing framework
 lsetup rucio         distributed data management system client
 lsetup views         Set up a full LCG release
 lsetup xcache        XRootD local proxy cache
 lsetup xrootd        XRootD data access
advancedTools        advanced tools menu
diagnostics          diagnostic tools menu
helpMe               more help
printMenu            show this menu
showVersions         show versions of installed software

[bash][atlas]:gpu-devel-env > asetup Athena,master,latest
Using Athena/22.0.18 [cmake] with platform x86_64-centos7-gcc8-opt
        at /cvmfs/atlas-nightlies.cern.ch/repo/sw/master_Athena_x86_64-centos7-gcc8-opt/2020-08-13T2101
[bash][atlas]:gpu-devel-env > echo $CXX
/opt/intel-clang/12.0.0/x86_64-centos7/bin/clang++
[bash][atlas]:gpu-devel-env > echo $CC
/opt/intel-clang/12.0.0/x86_64-centos7/bin/clang
[bash][atlas]:gpu-devel-env > which cmake
/opt/cmake/3.18.3/Linux-x86_64/bin/cmake
[bash][atlas]:gpu-devel-env >
```
